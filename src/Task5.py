def num_of_sublists(lists):
    count = 0

    for item in lists:
        if isinstance(item, list):
            count += 1
            count += num_of_sublists(item)

    return count