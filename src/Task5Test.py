import unittest
import Task5


class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(Task5.num_of_sublists([[1, 2, 3], [1, 2, 3], [1, 2, 3]]), 3)
        self.assertEqual(
            Task5.num_of_sublists([
                [[1, 2, 3], [1, 2, 3], [1, 2, 3]],
                [[1, 2, 3], [1, 2, 3], [1, 2, 3]]
                ]
            ),
            8
        )
        self.assertEqual(Task5.num_of_sublists([[1, 2, 3]]), 1)
        self.assertEqual(Task5.num_of_sublists([1, 2, 3]), 0)
        self.assertEqual(Task5.num_of_sublists([[1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3]]), 4)


if __name__ == '__main__':
    unittest.main()
